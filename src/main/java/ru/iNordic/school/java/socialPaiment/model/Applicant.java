package ru.iNordic.school.java.socialPaiment.model;

public class Applicant {
    String name;
    String lastName;
    int familyMembers;
    int age;
    int totalRevenue;
    int revenuePerPerson;
    String revenueLevel;
    int moneyedAssistance;
    boolean approval;
    Integer applicationNumber;


    public String getRevenueLevel() {
        return revenueLevel;
    }

    public void setRevenueLevel(String revenueLevel) {
        this.revenueLevel = revenueLevel;
    }

    public enum Enum {
        LOW, MIDDLE, UPPER;
    }

    public int getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(int applicationNumber) {
        this.applicationNumber = applicationNumber++;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRevenuePerPerson() {
        return revenuePerPerson;
    }

    public void setRevenuePerPerson(int revenuePerPerson) {
        this.revenuePerPerson = revenuePerPerson;
    }

    public int getMoneyedAssistance() {
        return moneyedAssistance;
    }

    public void setMoneyedAssistance(int moneyedAssistance) {
        this.moneyedAssistance = moneyedAssistance;
    }

    public boolean isApproval() {
        return approval;
    }

    public void setApproval(boolean approval) {
        this.approval = approval;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getFamilyMembers() {
        return familyMembers;
    }

    public void setFamilyMembers(int familyMembers) {
        this.familyMembers = familyMembers;
    }

    public int getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(int totalRevenue) {
        this.totalRevenue = totalRevenue;
    }
}
