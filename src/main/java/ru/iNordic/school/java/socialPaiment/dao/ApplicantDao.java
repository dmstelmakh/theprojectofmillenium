package ru.iNordic.school.java.socialPaiment.dao;

import ru.iNordic.school.java.socialPaiment.model.Applicant;

import java.util.Set;

public interface ApplicantDao {
    boolean check(Integer applicationNumber);

    void add(Applicant applicant) throws IllegalArgumentException;

    void delete(Integer applicationNumber) throws IllegalArgumentException;

    void update(Applicant applicant) throws IllegalArgumentException;

    Applicant get(int applicationNumber) throws IllegalArgumentException;

    default boolean aproved() {
        return false;
    }

    Set<Applicant> getAll();

 }
