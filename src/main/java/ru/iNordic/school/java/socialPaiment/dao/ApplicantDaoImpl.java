package ru.iNordic.school.java.socialPaiment.dao;

import org.springframework.beans.factory.annotation.Autowired;
import ru.iNordic.school.java.socialPaiment.model.Applicant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ApplicantDaoImpl implements ApplicantDao {
    private final Map<Integer, Applicant> map;
    private LoggerUtil loggerUtil = null;

    @Autowired
    public ApplicantDaoImpl(Map<Integer, Applicant> map) {
        this.map = map;
    }

    public ApplicantDaoImpl() {
        this(new HashMap<>());
    }

    public Map<Integer, Applicant> getMap() {
        return map;
    }

    @Autowired
    public void setLoggerUtil(LoggerUtil loggerUtil) {
        this.loggerUtil = loggerUtil;
    }

    @Override
    public boolean check(Integer applicationNumber) {
        return map.containsKey(applicationNumber);
    }

    @Override
    public void add(Applicant applicant) throws IllegalArgumentException {

        if (check(applicant.getApplicationNumber())) {
            throw new IllegalArgumentException();
        }
        map.put(applicant.getApplicationNumber(), applicant);
        if (loggerUtil != null) {
            loggerUtil.printMessage("add from dao");
        }
      }

    @Override
    public void delete(Integer applicationNumber) throws IllegalArgumentException {
        if (!check(applicationNumber)) {
            throw new IllegalArgumentException();
        }
        map.remove(applicationNumber);
    }

    @Override
    public void update(Applicant applicant) throws IllegalArgumentException {
        if (!check(applicant.getApplicationNumber())) {
            throw new IllegalArgumentException();
        }
        map.put(applicant.getApplicationNumber(), applicant);

    }

    @Override
    public Applicant get(int applicationNumber) throws IllegalArgumentException {
        if (!check(applicationNumber)) {
            throw new IllegalArgumentException();
        }
        return map.get(applicationNumber);
    }

    @Override
    public boolean aproved() {
        return true;
    }

    @Override
    public Set<Applicant> getAll() {
        return new HashSet<>(map.values());
    }
}
